import { HandshakerResponse } from '../service/model/handshaker-response';

/**
 * Gather the informations sent by a handshaker response.
 */
export type RtSession = HandshakerResponse;
