import { SocketServerInterface } from './socket-server.interface';

export interface WebSocketProxyInterface {
    uri: string;
    host: string;
    port: number;
    client: string;
    socket: SocketServerInterface;
}
