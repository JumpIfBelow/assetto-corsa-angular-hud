export enum Datagram {
  UDP4 = "udp4",
  UDP6 = "udp6",
  TCP = "tcp",
}
