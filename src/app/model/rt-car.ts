import { RtCarInfo } from '../service/model/rt-car-info';

/**
 * All car informations are gathered in this class.
 */
export type RtCar = RtCarInfo;
