type DeltaLaps = {
    previousLap: LapTime;
    bestLap: LapTime;
    lastLap: LapTime;
};

export class LapTime {
    public index: number;
    public lap: number;
    public duration: number;
    public deltaLaps?: DeltaLaps;
    public previousLap?: LapTime;
    public bestLap?: LapTime;
    public lastLap?: LapTime;

    public constructor(index: number, lap: number, duration: number) {
        this.index = index;
        this.lap = lap;
        this.duration = duration;
    }

    public getDeltaPrevious(): number | undefined {
        if (!this.previousLap) {
            return undefined;
        }

        return this.duration - this.previousLap.duration;
    }

    public getDeltaBest(): number | undefined {
        if (!this.bestLap) {
            return undefined;
        }

        return this.duration - this.bestLap.duration;
    }

    public getDeltaLast(): number | undefined {
        if (!this.lastLap) {
            return undefined;
        }

        return this.duration - this.lastLap.duration;
    }
}
