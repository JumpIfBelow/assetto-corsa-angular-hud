import { RtLapInfo } from '../service/model/rt-lap-info';

/**
 * All lap informations are gathered in this class.
 */
export type RtLap = RtLapInfo;
