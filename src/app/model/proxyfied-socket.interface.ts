import { WebSocketSubject } from 'rxjs/webSocket';
import { WebSocketProxyInterface } from './web-socket-proxy.interface';

export interface ProxyfiedSocketInterface {
    webSocketProxy: WebSocketProxyInterface;
    webSocket: WebSocketSubject<ArrayBuffer>;
}
