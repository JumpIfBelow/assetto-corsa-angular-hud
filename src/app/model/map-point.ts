export class MapPoint {
    public readonly speedKph: number;
    public readonly gas: number;
    public readonly brake: number;
    public readonly xG: number;
    public readonly yG: number;
    public readonly zG: number;
    public readonly x: number;
    public readonly y: number;
    public readonly z: number;

    public constructor(
        speedKph: number,
        gas: number,
        brake: number,
        xG: number,
        yG: number,
        zG: number,
        x: number,
        y: number,
        z: number,
    ) {
        this.speedKph = speedKph;
        this.gas = gas;
        this.brake = brake;
        this.xG = xG;
        this.yG = yG;
        this.zG = zG;
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
