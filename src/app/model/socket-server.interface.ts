import { Datagram } from "./datagram.enum";

export interface SocketServerInterface {
  host: string;
  port: number;
  datagram: Datagram;
}
