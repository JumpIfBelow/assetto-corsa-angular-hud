import { MapPoint } from "./map-point";

export class LapPoints {
    
  private mapPoints: {[lap: number]: MapPoint[]};

  public constructor() {
    this.mapPoints = {};
  }

  public appendPoint(
      lap: number,
      mapPoint: MapPoint,
    ) {
      if (this.mapPoints[lap] === undefined) {
          this.mapPoints[lap] = [];
      }

      this.mapPoints[lap].push(mapPoint);
  }

  public getLap(lap: number): MapPoint[] {
    return this.mapPoints[lap];
  }

  public getStoredLaps(): number[] {
      const properties: number[] = [];

      for (const property in this.mapPoints) {
          properties.push(+property);
      }

      return properties;
  }
}
