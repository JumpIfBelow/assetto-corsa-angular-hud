import { Datagram } from '../datagram.enum';

interface Server {
    host: string;
    port: number;
    datagram: Datagram;
}

export interface Configuration {
    webSocketApiEndpoint: string;
    assettoCorsaServer: Server;
}
