import { RtSession } from './rt-session';
import { RtLap } from './rt-lap';
import { RtCar } from './rt-car';
import { Observable } from 'rxjs';

export interface Session {
    rtSession$: Observable<RtSession>;
    rtCar$: Observable<RtCar>;
    rtLap$: Observable<RtLap>;
}
