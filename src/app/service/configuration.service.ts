import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Configuration } from '../model/configuration/configuration';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class ConfigurationService {
    protected readonly http = inject(HttpClient);
    private configuration$!: Observable<Configuration>;

    public constructor() {
        this.init();
    }

    public configuration(): Observable<Configuration> {
        return this.configuration$;
    }

    private init(): void {
        this.configuration$ = this.http
            .get<Configuration>('/assets/config.json')
            .pipe(shareReplay(1));
    }
}
