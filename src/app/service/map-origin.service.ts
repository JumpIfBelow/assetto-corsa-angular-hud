import { Injectable, inject } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { RtDataService } from './rt-data.service';
import { first } from 'rxjs/operators';
import { TrackName } from '../model/track-name';

@Injectable()
export class MapOriginService {
    protected readonly rtDataService = inject(RtDataService);

    public setOrigin(camera: THREE.Camera, controls: OrbitControls): void {
        this.rtDataService.rtSession$.pipe(first()).subscribe((rtSession) => {
            let vector: { x: number; y: number; z: number };

            switch (rtSession.trackName) {
                case TrackName.SPA:
                    vector = { x: 300, y: 50, z: 200 };
                    break;

                default:
                    vector = { x: 0, y: 0, z: 0 };
            }

            camera.position.set(vector.x, vector.y, vector.z);
            controls.target.set(vector.x, vector.y, vector.z);

            controls.update();
        });
    }
}
