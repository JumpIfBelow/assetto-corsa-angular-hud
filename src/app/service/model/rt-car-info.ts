/**
 * Gathers the information describing car status.
 */
export class RtCarInfo {
    public static readonly LENGTH = 328;

    /**
     * char 8 bits
     */
    public readonly identifier!: string;

    /**
     * ? 24 bits
     */
    public readonly unknown1!: any;

    /**
     * int 32 bits
     */
    public readonly size!: number;

    /**
     * float 32 bits
     */
    public readonly speedKph!: number;

    /**
     * float 32 bits
     */
    public readonly speedMph!: number;

    /**
     * float 32 bits
     */
    public readonly speedMps!: number;

    /**
     * bool 8 bits
     */
    public readonly isAbsEnabled!: boolean;

    /**
     * bool 8 bits
     */
    public readonly isAbsInAction!: boolean;

    /**
     * bool 8 bits
     */
    public readonly isTcInAction!: boolean;

    /**
     * bool 8 bits
     */
    public readonly isTcEnabled!: boolean;

    /**
     * bool 8 bits
     */
    public readonly isInPit!: boolean;

    /**
     * bool 8 bits
     */
    public readonly isEngineLimiterOn!: boolean;

    /**
     * ? 16 bits
     */
    public readonly unknown2!: any;

    /**
     * float 32 bits
     */
    public readonly verticalG!: number;

    /**
     * float 32 bits
     */
    public readonly horizontalG!: number;

    /**
     * float 32 bits
     */
    public readonly frontalG!: number;

    /**
     * int 32 bits
     */
    public readonly lapTime!: number;

    /**
     * int 32 bits
     */
    public readonly lastLap!: number;

    /**
     * int 32 bits
     */
    public readonly bestLap!: number;

    /**
     * int 32 bits
     */
    public readonly lapCount!: number;

    /**
     * ? 16 bits
     */
    public readonly unknown3!: any;

    /**
     * float 32 bits
     */
    public readonly gas!: number;

    /**
     * float 32 bits
     */
    public readonly brake!: number;

    /**
     * float 32 bits
     */
    public readonly clutch!: number;

    /**
     * float 32 bits
     */
    public readonly engineRPM!: number;

    /**
     * float 32 bits
     */
    public readonly steer!: number;

    /**
     * int 32 bits
     */
    public readonly gear!: number;

    /**
     * float 32 bits
     */
    public readonly cgHeight!: number;

    /**
     * float[4] 32 bits
     */
    public readonly wheelAngularSpeed!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly slipAngle!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly slipAngleContactPatch!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly slipRatio!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly tyreSlip!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly ndSlip!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly load!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly Dy!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly Mz!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly tyreDirtyLevel!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly camberRAD!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly tyreRadius!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly tyreLoadedRadius!: number[];

    /**
     * float[4] 32 bits
     */
    public readonly suspensionHeight!: number[];

    /**
     * float 32 bits
     */
    public readonly carPositionNormalized!: number;

    /**
     * float 32 bits
     */
    public readonly carSlope!: number;

    /**
     * float[3] 32 bits
     */
    public readonly carCoordinates!: number[];

    constructor(props: RtCarInfo) {
        Object.assign(this, props);
    }

    public static fromBuffer(buffer: ArrayBuffer): RtCarInfo {
        let view = new DataView(buffer);

        return new RtCarInfo({
            identifier: String.fromCharCode(view.getInt8(0)),
            unknown1: new Uint8Array(buffer.slice(1, 4)),
            size: view.getInt32(4, true),
            speedKph: view.getFloat32(8, true),
            speedMph: view.getFloat32(12, true),
            speedMps: view.getFloat32(16, true),
            isAbsEnabled: !!view.getInt8(20),
            isAbsInAction: !!view.getInt8(21),
            isTcInAction: !!view.getInt8(22),
            isTcEnabled: !!view.getInt8(23),
            isInPit: !!view.getInt8(24),
            isEngineLimiterOn: !!view.getInt8(25),
            unknown2: new Uint8Array(buffer.slice(26, 28)),
            verticalG: view.getFloat32(28, true),
            horizontalG: view.getFloat32(32, true),
            frontalG: view.getFloat32(36, true),
            lapTime: view.getInt32(40, true),
            lastLap: view.getInt32(44, true),
            bestLap: view.getInt32(48, true),
            lapCount: view.getInt32(52, true),
            unknown3: new Uint8Array(buffer.slice(54, 56)),
            gas: view.getFloat32(56, true),
            brake: view.getFloat32(60, true),
            clutch: view.getFloat32(64, true),
            engineRPM: view.getFloat32(68, true),
            steer: view.getFloat32(72, true),
            gear: view.getInt32(76, true),
            cgHeight: view.getFloat32(80, true),
            wheelAngularSpeed: [
                view.getFloat32(84, true),
                view.getFloat32(88, true),
                view.getFloat32(92, true),
                view.getFloat32(96, true),
            ],
            slipAngle: [
                view.getFloat32(100, true),
                view.getFloat32(104, true),
                view.getFloat32(108, true),
                view.getFloat32(112, true),
            ],
            slipAngleContactPatch: [
                view.getFloat32(116, true),
                view.getFloat32(120, true),
                view.getFloat32(124, true),
                view.getFloat32(128, true),
            ],
            slipRatio: [
                view.getFloat32(132, true),
                view.getFloat32(136, true),
                view.getFloat32(140, true),
                view.getFloat32(144, true),
            ],
            tyreSlip: [
                view.getFloat32(148, true),
                view.getFloat32(152, true),
                view.getFloat32(156, true),
                view.getFloat32(160, true),
            ],
            ndSlip: [
                view.getFloat32(164, true),
                view.getFloat32(168, true),
                view.getFloat32(172, true),
                view.getFloat32(176, true),
            ],
            load: [
                view.getFloat32(180, true),
                view.getFloat32(184, true),
                view.getFloat32(188, true),
                view.getFloat32(192, true),
            ],
            Dy: [
                view.getFloat32(196, true),
                view.getFloat32(200, true),
                view.getFloat32(204, true),
                view.getFloat32(208, true),
            ],
            Mz: [
                view.getFloat32(212, true),
                view.getFloat32(216, true),
                view.getFloat32(220, true),
                view.getFloat32(224, true),
            ],
            tyreDirtyLevel: [
                view.getFloat32(228, true),
                view.getFloat32(232, true),
                view.getFloat32(236, true),
                view.getFloat32(240, true),
            ],
            camberRAD: [
                view.getFloat32(244, true),
                view.getFloat32(248, true),
                view.getFloat32(252, true),
                view.getFloat32(256, true),
            ],
            tyreRadius: [
                view.getFloat32(260, true),
                view.getFloat32(264, true),
                view.getFloat32(268, true),
                view.getFloat32(272, true),
            ],
            tyreLoadedRadius: [
                view.getFloat32(276, true),
                view.getFloat32(280, true),
                view.getFloat32(284, true),
                view.getFloat32(288, true),
            ],
            suspensionHeight: [
                view.getFloat32(292, true),
                view.getFloat32(296, true),
                view.getFloat32(300, true),
                view.getFloat32(304, true),
            ],
            carPositionNormalized: view.getFloat32(308, true),
            carSlope: view.getFloat32(312, true),
            carCoordinates: [
                view.getFloat32(316, true),
                view.getFloat32(320, true),
                view.getFloat32(324, true),
            ],
        });
    }
}
