import { sanitize } from '../../function/sanitize.function';

/**
 * Gathers the information describing lap status.
 */
export class RtLapInfo {
    public static readonly LENGTH = 212;

    /**
     * int 32 bits
     */
    public readonly carIdentifierNumber!: number;

    /**
     * int 32 bits
     */
    public readonly lap!: number;

    /**
     * char[50] 8 bits
     */
    public readonly driverName!: string;

    /**
     * char[50] 8 bits
     */
    public readonly carName!: string;

    /**
     * int 32 bits
     */
    public readonly time!: number;

    constructor(props: RtLapInfo) {
        Object.assign(this, props);
    }

    public static fromBuffer(buffer: ArrayBuffer): RtLapInfo {
        let view = new DataView(buffer);
        let decoder = new TextDecoder();

        return new RtLapInfo({
            carIdentifierNumber: view.getInt32(0, true),
            lap: view.getInt32(4, true),
            driverName: sanitize(
                decoder.decode(new Uint8Array(buffer.slice(8, 108))),
            ),
            carName: sanitize(
                decoder.decode(new Uint8Array(buffer.slice(108, 208))),
            ),
            time: view.getInt32(208, true),
        });
    }
}
