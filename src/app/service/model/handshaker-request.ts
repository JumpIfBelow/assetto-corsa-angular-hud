export enum HandshackerIdentifier {
    EL_PHONE_DEVICE = 0,
    EL_PAD_DEVICE = 1,
    E_ANDROID_PHONE = 2,
    E_ANDROID_TABLET = 3,
}

export enum HandshackerVersion {
    FIRST = 1,
}

export enum HandshackerOperationId {
    HANDHSAKE = 0,
    SUBSCRIBE_UPDATE = 1,
    SUBSCRIBE_SPOT = 2,
    DISMISS = 3,
}

/**
 * Gathers the information sent by a handshaker request.
 */
export class HandshakerRequest {
    public static readonly LENGTH = 12;

    /**
     * int 32 bits
     */
    public readonly identifier!: HandshackerIdentifier;

    /**
     * int 32 bits
     */
    public readonly version!: HandshackerVersion;

    /**
     * int 32 bits
     */
    public readonly operationId!: HandshackerOperationId;

    constructor(props: Omit<HandshakerRequest, 'toBuffer'>) {
        Object.assign(this, props);
    }

    public toBuffer(): ArrayBuffer {
        const handshakePacket = new ArrayBuffer(HandshakerRequest.LENGTH);
        const view = new DataView(handshakePacket);

        view.setInt32(0, this.identifier, true);
        view.setInt32(4, this.version, true);
        view.setInt32(8, this.operationId, true);

        return handshakePacket;
    }
}
