import { sanitize } from '../../function/sanitize.function';

/**
 * Gathers the information sent by a handshaker response.
 */
export class HandshakerResponse {
    public static readonly LENGTH = 408;

    /**
     * char[50] 8 bits
     */
    public readonly carName!: string;

    /**
     * char[50] 8 bits
     */
    public readonly driverName!: string;

    /**
     * int 32 bits
     */
    public readonly identifier!: number;

    /**
     * int 32 bits
     */
    public readonly version!: number;

    /**
     * char[50] 32 bits
     */
    public readonly trackName!: string;

    /**
     * char[50] 32 bits
     */
    public readonly trackConfig!: string;

    constructor(props: HandshakerResponse) {
        Object.assign(this, props);
    }

    public static fromBuffer(arrayBuffer: ArrayBuffer): HandshakerResponse {
        const dataView = new DataView(arrayBuffer);

        return new HandshakerResponse({
            carName: sanitize(
                new TextDecoder().decode(arrayBuffer.slice(0, 100)),
            ),
            driverName: sanitize(
                new TextDecoder().decode(arrayBuffer.slice(100, 200)),
            ),
            identifier: dataView.getInt32(200, true),
            version: dataView.getInt32(204, true),
            trackName: sanitize(
                new TextDecoder().decode(arrayBuffer.slice(208, 308)),
            ),
            trackConfig: sanitize(
                new TextDecoder().decode(arrayBuffer.slice(308, 408)),
            ),
        });
    }
}
