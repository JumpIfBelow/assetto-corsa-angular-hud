import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, map, switchMap } from 'rxjs';
import { ConfigurationService } from './configuration.service';
import { SocketServerInterface } from '../model/socket-server.interface';
import { WebSocketProxyInterface } from '../model/web-socket-proxy.interface';
import { ProxyfiedSocketInterface } from '../model/proxyfied-socket.interface';
import { WebSocketSubject } from 'rxjs/webSocket';

@Injectable()
export class WebSocketProxyService {
    protected readonly configurationService = inject(ConfigurationService);
    protected readonly http = inject(HttpClient);

    public createWebSocket(
        socketServer: SocketServerInterface,
    ): Observable<ProxyfiedSocketInterface> {
        return this.configurationService.configuration().pipe(
            map((configuration) => configuration.webSocketApiEndpoint),
            switchMap((endpoint) =>
                this.http.post<WebSocketProxyInterface>(
                    `${endpoint}/socket-proxies`,
                    socketServer,
                ),
            ),
            map((webSocketProxy) => ({
                webSocketProxy,
                webSocket: new WebSocketSubject<ArrayBuffer>({
                    url: webSocketProxy.uri,
                    serializer: (data) => data,
                    deserializer: (e) => e.data,
                    binaryType: 'arraybuffer',
                }),
            })),
        );
    }

    public closeWebSocket(
        proxyfiedSocket: ProxyfiedSocketInterface,
    ): Observable<void> {
        proxyfiedSocket.webSocket.complete();

        return this.configurationService.configuration().pipe(
            map((configuration) => configuration.webSocketApiEndpoint),
            switchMap((endpoint) =>
                this.http.delete<void>(
                    `${endpoint}/socket-proxies/${proxyfiedSocket.webSocketProxy.port}`,
                ),
            ),
        );
    }
}
