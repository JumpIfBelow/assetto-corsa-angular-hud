import {
    Observable,
    filter,
    first,
    map,
    mergeMap,
    shareReplay,
    switchMap,
    tap,
} from 'rxjs';
import {
    HandshackerIdentifier,
    HandshackerOperationId,
    HandshackerVersion,
    HandshakerRequest,
} from './model/handshaker-request';
import { HandshakerResponse } from './model/handshaker-response';
import { RtCarInfo } from './model/rt-car-info';
import { Injectable, inject } from '@angular/core';
import { WebSocketProxyService } from './web-socket-proxy.service';
import { WebSocketSubject } from 'rxjs/webSocket';
import { RtLapInfo } from './model/rt-lap-info';
import { Session } from '../model/session';
import { ProxyfiedSocketInterface } from '../model/proxyfied-socket.interface';
import { SocketServerInterface } from '../model/socket-server.interface';

@Injectable()
export class AssettoCorsaCommunication {
    public get handshakerResponse$(): Observable<HandshakerResponse> {
        return this._handshakerResponse$!;
    }

    public get rtCarInfo$(): Observable<RtCarInfo> {
        return this._rtCarInfo$!;
    }

    public get rtLapInfo$(): Observable<RtLapInfo> {
        return this._rtLapInfo$!;
    }

    protected readonly webSocketProxyService = inject(WebSocketProxyService);

    private proxyfiedSocketServer$?: Observable<ProxyfiedSocketInterface>;
    private webSocket$?: Observable<WebSocketSubject<ArrayBuffer>>;

    private _handshakerResponse$?: Observable<HandshakerResponse>;
    private _rtCarInfo$?: Observable<RtCarInfo>;
    private _rtLapInfo$?: Observable<RtLapInfo>;

    public initCommunication(socketServer: SocketServerInterface): Session {
        this.proxyfiedSocketServer$ = this.webSocketProxyService
            .createWebSocket(socketServer)
            .pipe(shareReplay(1));

        this.webSocket$ = this.proxyfiedSocketServer$.pipe(
            map(({ webSocket }) => webSocket),
            shareReplay(1),
        );

        const data$ = this.webSocket$.pipe(
            switchMap((webSocket) => webSocket),
            shareReplay(1),
        );

        this._handshakerResponse$ = data$.pipe(
            filter((data) => data.byteLength === HandshakerResponse.LENGTH),
            map((data) => HandshakerResponse.fromBuffer(data)),
            shareReplay(1),
        );

        this._rtCarInfo$ = data$.pipe(
            filter((data) => data.byteLength === RtCarInfo.LENGTH),
            map((data) => RtCarInfo.fromBuffer(data)),
            shareReplay(1),
        );

        this._rtLapInfo$ = data$.pipe(
            filter((data) => data.byteLength === RtLapInfo.LENGTH),
            map((data) => RtLapInfo.fromBuffer(data)),
            shareReplay(1),
        );

        this.webSocket$.pipe(first()).subscribe((webSocket) => {
            // handshake
            const handshakePacket = new HandshakerRequest({
                identifier: HandshackerIdentifier.EL_PHONE_DEVICE,
                version: HandshackerVersion.FIRST,
                operationId: HandshackerOperationId.HANDHSAKE,
            });

            webSocket.next(handshakePacket.toBuffer());

            // init car
            const subscribeCarPacket = new HandshakerRequest({
                identifier: HandshackerIdentifier.EL_PHONE_DEVICE,
                version: HandshackerVersion.FIRST,
                operationId: HandshackerOperationId.SUBSCRIBE_UPDATE,
            });

            webSocket.next(subscribeCarPacket.toBuffer());

            // init lap
            const subscribeLapPacket = new HandshakerRequest({
                identifier: HandshackerIdentifier.EL_PHONE_DEVICE,
                version: HandshackerVersion.FIRST,
                operationId: HandshackerOperationId.SUBSCRIBE_SPOT,
            });
            webSocket.next(subscribeLapPacket.toBuffer());
        });

        return {
            rtSession$: this._handshakerResponse$,
            rtCar$: this._rtCarInfo$,
            rtLap$: this._rtLapInfo$,
        };
    }

    public stopCommunication(): void {
        if (!this.proxyfiedSocketServer$) {
            throw new Error('Communication not started');
        }

        this.proxyfiedSocketServer$
            .pipe(
                first(),
                tap(({ webSocket }) => {
                    const dismissPacket = new HandshakerRequest({
                        identifier: HandshackerIdentifier.EL_PHONE_DEVICE,
                        version: HandshackerVersion.FIRST,
                        operationId: HandshackerOperationId.DISMISS,
                    });
                    webSocket.next(dismissPacket.toBuffer());
                }),
                mergeMap((proxyfiedSocket) =>
                    this.webSocketProxyService.closeWebSocket(proxyfiedSocket),
                ),
            )
            .subscribe(() => {
                this.proxyfiedSocketServer$ = undefined;
                this.webSocket$ = undefined;
                this._handshakerResponse$ = undefined;
                this._rtCarInfo$ = undefined;
                this._rtLapInfo$ = undefined;
            });
    }
}
