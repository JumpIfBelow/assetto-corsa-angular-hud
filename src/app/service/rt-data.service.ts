import { Injectable, inject } from '@angular/core';
import { RtCar } from '../model/rt-car';
import { Observable } from 'rxjs';
import { shareReplay, map, mergeMap, first } from 'rxjs/operators';
import { RtSession } from '../model/rt-session';
import { RtLap } from '../model/rt-lap';
import { ConfigurationService } from './configuration.service';
import { AssettoCorsaCommunication as AssettoCorsaCommunicationService } from './assetto-corsa-communication.service';

@Injectable()
export class RtDataService {
    public get rtSession$(): Observable<RtSession> {
        return this._rtSession$;
    }

    public get rtLap$(): Observable<RtLap> {
        return this._rtLap$;
    }

    public get rtCar$(): Observable<RtCar> {
        return this._rtCar$;
    }

    protected readonly configurationService = inject(ConfigurationService);
    protected readonly assettoCorsaCommunicationService = inject(
        AssettoCorsaCommunicationService,
    );

    private _rtSession$!: Observable<RtSession>;
    private _rtLap$!: Observable<RtLap>;
    private _rtCar$!: Observable<RtCar>;

    public constructor() {
        this.init();
    }

    private init(): void {
        const configuration$ = this.configurationService.configuration();
        const session$ = configuration$.pipe(
            first(),
            map(({ assettoCorsaServer }) => assettoCorsaServer),
            map((assettoCorsaServer) =>
                this.assettoCorsaCommunicationService.initCommunication(
                    assettoCorsaServer,
                ),
            ),
            shareReplay(1),
        );

        this._rtSession$ = session$.pipe(
            mergeMap(({ rtSession$ }) => rtSession$),
            shareReplay(1),
        );

        this._rtLap$ = session$.pipe(
            mergeMap(({ rtLap$ }) => rtLap$),
            shareReplay(1),
        );

        this._rtCar$ = session$.pipe(
            mergeMap(({ rtCar$ }) => rtCar$),
            shareReplay(1),
        );
    }
}
