import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HudComponent } from './hud/hud.component';
import { SpeedometerComponent } from './dashboard/speedometer/speedometer.component';
import { TachometerComponent } from './dashboard/tachometer/tachometer.component';
import { PedalsComponent } from './dashboard/pedals/pedals.component';
import { LapsComponent } from './dashboard/laps/laps.component';
import { DurationComponent } from './utils/duration/duration.component';
import { GearsComponent } from './dashboard/gears/gears.component';
import { ElectronicsComponent } from './dashboard/electronics/electronics.component';
import { SteeringWheelComponent } from './dashboard/steering-wheel/steering-wheel.component';
import { WheelsSpeedComponent } from './dashboard/wheels-speed/wheels-speed.component';
import { GMeterComponent } from './dashboard/g-meter/g-meter.component';
import { TimetableComponent } from './dashboard/timetable/timetable.component';
import { TimeDeltaComponent } from './utils/time-delta/time-delta.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { LapMapComponent } from './dashboard/lap-map/lap-map.component';
import { FormsModule } from '@angular/forms';
import { GaugeComponent } from './hud/gauge/gauge.component';
import { SelectorComponent } from './hud/selector/selector.component';
import { AssettoCorsaCommunication as AssettoCorsaCommunicationService } from './service/assetto-corsa-communication.service';
import { ConfigurationService } from './service/configuration.service';
import { MapOriginService } from './service/map-origin.service';
import { RtDataService } from './service/rt-data.service';
import { WebSocketProxyService } from './service/web-socket-proxy.service';

@NgModule({
    declarations: [
        AppComponent,
        HudComponent,
        SpeedometerComponent,
        TachometerComponent,
        PedalsComponent,
        LapsComponent,
        DurationComponent,
        GearsComponent,
        ElectronicsComponent,
        SteeringWheelComponent,
        WheelsSpeedComponent,
        GMeterComponent,
        TimetableComponent,
        TimeDeltaComponent,
        LapMapComponent,
        GaugeComponent,
        SelectorComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        FormsModule,
    ],
    providers: [
        AssettoCorsaCommunicationService,
        ConfigurationService,
        MapOriginService,
        RtDataService,
        WebSocketProxyService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
