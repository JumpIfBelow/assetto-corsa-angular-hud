export enum GaugeType {
    ANALOG = 0,
    NUMERIC = 1,
    BARS = 2,
}
