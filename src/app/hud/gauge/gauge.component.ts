import { Component, OnInit, Input } from '@angular/core';
import { GaugeType } from './gauge-type';

@Component({
    selector: 'app-gauge',
    templateUrl: './gauge.component.html',
    styleUrls: ['./gauge.component.scss'],
})
export class GaugeComponent implements OnInit {
    @Input({ required: true }) public type!: GaugeType;

    @Input({ required: true }) public value!: number;
    @Input({ required: true }) public min!: number;
    @Input({ required: true }) public max!: number;
    @Input({ required: true }) public low!: number;
    @Input({ required: true }) public high!: number;
    @Input({ required: true }) public optimum!: number;

    @Input({ required: true }) public unit!: string;
    @Input() public isCriticalLow: boolean = false;
    @Input() public isCriticalHigh: boolean = false;

    public GaugeType = GaugeType;

    constructor() {}

    ngOnInit() {}

    public isLow(): boolean {
        return this.value <= this.low;
    }

    public isHigh(): boolean {
        return this.value >= this.high;
    }

    public isOptimum(): boolean {
        return (
            (this.value <= this.low &&
                this.value >= this.optimum &&
                this.optimum <= this.low) ||
            (this.value >= this.high &&
                this.value <= this.optimum &&
                this.optimum >= this.high) ||
            (this.value <= this.high &&
                this.value >= this.low &&
                this.optimum <= this.high &&
                this.optimum >= this.low)
        );
    }

    public isNormal(): boolean {
        return !this.isLow() && !this.isHigh() && !this.isOptimum();
    }

    public rotateTurn(value: number): number {
        const startRotation = 0.125;
        const endRotation = 1 - startRotation * 2;
        const range = this.max - this.min;

        return startRotation + ((value - this.min) / range) * endRotation;
    }
}
