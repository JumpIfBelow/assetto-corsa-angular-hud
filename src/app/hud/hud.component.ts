import { Component, OnInit, inject } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RtDataService } from '../service/rt-data.service';
import { RtCar } from '../model/rt-car';
import { RtSession } from '../model/rt-session';
import { RtLap } from '../model/rt-lap';

@Component({
    selector: 'app-hud',
    templateUrl: './hud.component.html',
    styleUrls: ['./hud.component.scss'],
})
export class HudComponent implements OnInit {
    public rtSession$!: Observable<RtSession>;
    public rtLap$!: Observable<RtLap>;
    public rtCar$!: Observable<RtCar>;
    public resetStatistics$: Subject<void> = new Subject();

    public componentList: Record<string, boolean> = {
        Speedometer: true,
        Tachometer: true,
        Gears: true,
        Electronics: true,
        Pedals: true,
        Laps: true,
        'Steering wheel': true,
        'Wheels speed': true,
        'G-meter': true,
        Timetable: true,
        'Lap map': false,
    };

    private readonly rtDataService = inject(RtDataService);

    public ngOnInit(): void {
        this.rtSession$ = this.rtDataService.rtSession$;
        this.rtLap$ = this.rtDataService.rtLap$;
        this.rtCar$ = this.rtDataService.rtCar$;
    }
}
