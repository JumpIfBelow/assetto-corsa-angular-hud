import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-selector',
    templateUrl: './selector.component.html',
    styleUrls: ['./selector.component.scss'],
})
export class SelectorComponent implements OnInit {
    @Input({ required: true }) public selectables!: Record<string, boolean>;

    public Object = Object;

    constructor() {}

    ngOnInit() {}
}
