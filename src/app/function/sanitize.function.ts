class Sanitizer {
  public static readonly NULL_BYTE = Object.freeze(/\0/g);
  public static readonly END_STRING_MARKER = Object.freeze(/%[\s\S]*$/);
}

export function sanitize(value: string): string {
  return value
    .replaceAll(Sanitizer.NULL_BYTE, "")
    .replaceAll(Sanitizer.END_STRING_MARKER, "");
}
