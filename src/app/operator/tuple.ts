import { pipe } from "rxjs";
import { map, skip } from "rxjs/operators";

export const tuple = function <T>(n: number) {
  const elements = new Array<T>(n);

  return pipe(
    map<T, T[]>(element => {
      elements.push(element);
      elements.shift();

      return elements;
    }),
    skip(n - 1),
  )
};
