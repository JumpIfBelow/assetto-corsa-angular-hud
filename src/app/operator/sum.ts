import { map } from "rxjs/operators";

export const sum = () => map<number[], number>(values => values.reduce((sum: number, value: number) => sum + value));
