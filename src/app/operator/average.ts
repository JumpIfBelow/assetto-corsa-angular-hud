import { pipe } from "rxjs";
import { tuple } from "./tuple";
import { map } from "rxjs/operators";
import { sum } from "./sum";

export const average = (count: number) => pipe(
    tuple<number>(count),
    sum(),
    map(sum => sum / count),
);
