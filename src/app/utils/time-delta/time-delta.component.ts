import { Component, OnInit, Input } from '@angular/core';

type TimeDelta = {
    isPositive: boolean;
    seconds: number;
    milliseconds: number;
};

@Component({
    selector: 'app-time-delta',
    templateUrl: './time-delta.component.html',
    styleUrls: ['./time-delta.component.scss'],
})
export class TimeDeltaComponent implements OnInit {
    @Input({ required: true }) public set milliseconds(milliseconds: number) {
        this.timeDelta = this.millisecondsToDelta(milliseconds);
    }

    public timeDelta!: TimeDelta;

    constructor() {}

    ngOnInit() {}

    private millisecondsToDelta(milliseconds: number): TimeDelta {
        const absMilliseconds = Math.abs(milliseconds);

        return {
            isPositive: milliseconds >= 0,
            seconds: Math.floor(absMilliseconds / 1000),
            milliseconds: absMilliseconds % 1000,
        };
    }
}
