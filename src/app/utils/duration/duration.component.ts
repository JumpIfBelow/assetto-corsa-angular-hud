import { Component, OnInit, Input } from '@angular/core';

type Duration = {
    minutes: number;
    seconds: number;
    milliseconds: number;
};

@Component({
    selector: 'app-duration',
    templateUrl: './duration.component.html',
    styleUrls: ['./duration.component.scss'],
})
export class DurationComponent implements OnInit {
    @Input({ required: true }) public set milliseconds(milliseconds: number) {
        this.duration = this.millisecondsToDuration(milliseconds);
    }

    public duration!: Duration;

    constructor() {}

    ngOnInit() {}

    private millisecondsToDuration(milliseconds: number): Duration {
        return {
            minutes: Math.floor(milliseconds / 1000 / 60),
            seconds: Math.floor((milliseconds / 1000) % 60),
            milliseconds: milliseconds % 1000,
        };
    }
}
