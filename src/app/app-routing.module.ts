import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HudComponent } from './hud/hud.component';

const routes: Routes = [
    {
        path: 'hud',
        component: HudComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
