import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-laps',
    templateUrl: './laps.component.html',
    styleUrls: ['./laps.component.scss'],
})
export class LapsComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public rtCar!: RtCar;

    constructor() {}

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => (this.rtCar = rtCar));
    }
}
