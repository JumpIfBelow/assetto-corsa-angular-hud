import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-steering-wheel',
    templateUrl: './steering-wheel.component.html',
    styleUrls: ['./steering-wheel.component.scss'],
})
export class SteeringWheelComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public readonly Math = Math;

    public steer: number = 0;

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => (this.steer = rtCar.steer));
    }
}
