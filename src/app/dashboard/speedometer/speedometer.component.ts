import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { pairwise, map, filter } from 'rxjs/operators';
import { RtCar } from '../../model/rt-car';
import { GaugeType } from '../../hud/gauge/gauge-type';

@Component({
    selector: 'app-speedometer',
    templateUrl: './speedometer.component.html',
    styleUrls: ['./speedometer.component.scss'],
})
export class SpeedometerComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;
    @Input({ required: true }) public resetStatistics$!: Observable<void>;

    public rtCar!: RtCar;
    public maxSpeedKph: number = 0;
    public acceleration: number = 0;

    public GaugeType = GaugeType;

    constructor() {
        this.maxSpeedKph = 0;
    }

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => {
            this.rtCar = rtCar;
            if (rtCar && rtCar.speedKph > this.maxSpeedKph) {
                this.maxSpeedKph = rtCar.speedKph;
            }
        });

        this.resetStatistics$.subscribe(() => {
            this.maxSpeedKph = 0;
        });

        this.rtCar$
            .pipe(
                map((rtCar) => ({
                    lapCount: rtCar.lapCount,
                    speedMps: rtCar.speedKph >= 0.1 ? rtCar.speedMps : 0,
                    lapTime: rtCar.lapTime / 1000,
                })),
                pairwise(),
                filter((pair) => pair[0].lapCount === pair[1].lapCount),
                map(
                    (pair) =>
                        (pair[1].speedMps - pair[0].speedMps) /
                        (pair[1].lapTime - pair[0].lapTime),
                ),
            )
            .subscribe((acceleration) => (this.acceleration = acceleration));
    }
}
