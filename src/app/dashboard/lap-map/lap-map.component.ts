import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    inject,
} from '@angular/core';
import { Observable, throwError } from 'rxjs';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { throttleTime } from 'rxjs/operators';
import { RtCar } from '../../model/rt-car';
import { LapPoints } from '../../model/lap-points';
import { MapOriginService } from '../../service/map-origin.service';
import { MapPoint } from '../../model/map-point';

@Component({
    selector: 'app-lap-map',
    templateUrl: './lap-map.component.html',
    styleUrls: ['./lap-map.component.scss'],
})
export class LapMapComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;
    public displayedLaps: number[];

    public camera?: THREE.PerspectiveCamera;
    public controls?: OrbitControls;
    public lapPoint: LapPoints;

    public readonly currentLap = -1;

    @ViewChild('scene', { static: true })
    private div!: ElementRef<HTMLDivElement>;

    private canvasSize = [800, 600];
    private scene?: THREE.Scene;
    private renderer?: THREE.Renderer;

    private maxSpeedKph: number;

    private readonly mapOriginService = inject(MapOriginService);

    public constructor() {
        this.displayedLaps = [-1];
        this.lapPoint = new LapPoints();
        this.maxSpeedKph = 0;
    }

    ngOnInit() {
        this.initRenderer();
        let lastLap: number;

        this.rtCar$.pipe(throttleTime(10)).subscribe((rtCar) => {
            if (rtCar.speedKph > this.maxSpeedKph) {
                this.maxSpeedKph = rtCar.speedKph;
            }

            this.lapPoint.appendPoint(
                rtCar.lapCount,
                new MapPoint(
                    rtCar.speedKph,
                    rtCar.gas,
                    rtCar.brake,
                    rtCar.verticalG,
                    rtCar.horizontalG,
                    rtCar.frontalG,
                    rtCar.carCoordinates[0],
                    rtCar.carCoordinates[1],
                    rtCar.carCoordinates[2],
                ),
            );

            if (lastLap !== rtCar.lapCount) {
                this.drawLaps();
            }

            lastLap = rtCar.lapCount;

            if (!this.displayedLaps.includes(this.currentLap)) {
                return;
            }

            const storedLaps = this.lapPoint.getStoredLaps();
            const mapPoints = this.lapPoint.getLap(
                storedLaps[storedLaps.length - 1],
            );
            const l = mapPoints.length;
            const a = mapPoints[l - 3];
            const b = mapPoints[l - 2];
            const c = mapPoints[l - 1];

            if (a && b && c) {
                this.drawLine(a, b, c);
            }
        });

        this.animate();
    }

    public drawLaps(): void {
        this.clearRender();

        for (const displayedLap of this.displayedLaps) {
            this.drawLap(displayedLap);
        }
    }

    private drawLap(lap: number): void {
        if (lap === this.currentLap) {
            const storedLaps = this.lapPoint.getStoredLaps();
            lap = storedLaps[storedLaps.length - 1];
        }

        const mapPoints = this.lapPoint.getLap(lap);
        for (const index in mapPoints) {
            const a = mapPoints[+index];
            const b = mapPoints[+index + 1];
            const c = mapPoints[+index + 2];

            if (a && b && c) {
                this.drawLine(a, b, c);
            }
        }
    }

    private drawLine(a: MapPoint, b: MapPoint, c: MapPoint): void {
        const positionVectors = [
            new THREE.Vector3(a.x, a.y, a.z),
            new THREE.Vector3(b.x, b.y, b.z),
            new THREE.Vector3(c.x, c.y, c.z),
        ];

        const accelerationVectors = [
            new THREE.Vector3(a.xG, a.yG, a.zG),
            new THREE.Vector3(b.xG, b.yG, b.zG),
            new THREE.Vector3(c.xG, c.yG, c.zG),
        ].map((accelerationVector, index) => {
            const positionVector = positionVectors[index];
            const quaternion = new THREE.Quaternion().setFromUnitVectors(
                new THREE.Vector3(0, 0, 0),
                positionVector,
            );
            return accelerationVector
                .applyQuaternion(quaternion)
                .add(positionVector);
        });
        const positionCurve = new THREE.CatmullRomCurve3(positionVectors);
        const accelerationCurve = new THREE.CatmullRomCurve3(
            accelerationVectors,
        );

        const positionGeometry = new THREE.BufferGeometry().setFromPoints(
            positionCurve.getPoints(20),
        );
        const accelerationGeometry = new THREE.BufferGeometry().setFromPoints(
            accelerationCurve.getPoints(20),
        );
        const averageG = this.average(
            ...[
                [a.xG, a.yG, a.zG],
                [b.xG, b.yG, b.zG],
                [c.xG, c.yG, c.zG],
            ].map(
                (gVector) => gVector.map(Math.abs).reduce((sum, g) => sum + g),
                0,
            ),
        );

        const colorFactor = averageG / 5;

        const positionMaterial = new THREE.LineBasicMaterial({
            color:
                (Math.floor(this.average(a.brake, b.brake, c.brake) * 0xff) <<
                    16) |
                (Math.floor(this.average(a.gas, b.gas, b.gas) * 0xff) << 8) |
                Math.floor(
                    (this.average(a.speedKph, b.speedKph, c.speedKph) /
                        this.maxSpeedKph) *
                        0xff,
                ),
        });

        const accelerationMaterial = new THREE.LineBasicMaterial({
            color:
                ((0xff * colorFactor) << 16) |
                ((0xff * colorFactor) << 8) |
                (0x50 * colorFactor),
        });

        const positionLine = new THREE.Line(positionGeometry, positionMaterial);
        const accelerationLine = new THREE.Line(
            accelerationGeometry,
            accelerationMaterial,
        );

        this.scene?.add(positionLine, accelerationLine);
    }

    public clearRender(): void {
        this.scene?.remove(...this.scene.children);
    }

    private initRenderer(): void {
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x424242);

        this.camera = new THREE.PerspectiveCamera(
            90,
            this.canvasSize[0] / this.canvasSize[1],
            1,
            10000,
        );

        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setSize(this.canvasSize[0], this.canvasSize[1]);

        this.controls = new OrbitControls(
            this.camera,
            this.renderer.domElement,
        );
        this.controls.enableDamping = true;
        this.controls.minDistance = 5;

        this.div.nativeElement.appendChild(this.renderer.domElement);

        this.mapOriginService.setOrigin(this.camera, this.controls);
    }

    private animate = (): void => {
        requestAnimationFrame(this.animate);

        this.controls?.update();

        this.renderer?.render(this.scene!, this.camera!);
    };

    private average(...numbers: number[]): number {
        return (
            numbers.reduce((previous, current) => previous + current) /
            numbers.length
        );
    }
}
