import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RtCar } from '../../model/rt-car';

interface Wheel {
    radius: number;
    angularSpeed: number;
    kph: number;
    synchronization: number;
}

@Component({
    selector: 'app-wheels-speed',
    templateUrl: './wheels-speed.component.html',
    styleUrls: ['./wheels-speed.component.scss'],
})
export class WheelsSpeedComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public Math: Math;

    public FLspeedKph?: number;
    public FRspeedKph?: number;
    public RRspeedKph?: number;
    public RLspeedKph?: number;

    public FLsynchronization?: number;
    public FRsynchronization?: number;
    public RRsynchronization?: number;
    public RLsynchronization?: number;

    constructor() {
        this.Math = Math;
    }

    ngOnInit() {
        const wheels$ = this.rtCar$.pipe(map((rtCar) => this.buildData(rtCar)));
        const FLwheel$ = wheels$.pipe(map((wheels) => wheels[0]));
        const FRwheel$ = wheels$.pipe(map((wheels) => wheels[1]));
        const RLwheel$ = wheels$.pipe(map((wheels) => wheels[2]));
        const RRwheel$ = wheels$.pipe(map((wheels) => wheels[3]));
        FLwheel$.pipe(map((wheel) => wheel.kph)).subscribe(
            (speedKph) => (this.FLspeedKph = speedKph),
        );

        FLwheel$.pipe(map((wheel) => wheel.synchronization)).subscribe(
            (synchronization) => (this.FLsynchronization = synchronization),
        );

        FRwheel$.pipe(map((wheel) => wheel.kph)).subscribe(
            (speedKph) => (this.FRspeedKph = speedKph),
        );

        FRwheel$.pipe(map((wheel) => wheel.synchronization)).subscribe(
            (synchronization) => (this.FRsynchronization = synchronization),
        );

        RLwheel$.pipe(map((wheel) => wheel.kph)).subscribe(
            (speedKph) => (this.RLspeedKph = speedKph),
        );

        RLwheel$.pipe(map((wheel) => wheel.synchronization)).subscribe(
            (synchronization) => (this.RLsynchronization = synchronization),
        );

        RRwheel$.pipe(map((wheel) => wheel.kph)).subscribe(
            (speedKph) => (this.RRspeedKph = speedKph),
        );

        RRwheel$.pipe(map((wheel) => wheel.synchronization)).subscribe(
            (synchronization) => (this.RRsynchronization = synchronization),
        );
    }

    private buildData(rtCar: RtCar): Wheel[] {
        const wheels: Wheel[] = [];

        for (let i = 0; i < rtCar.wheelAngularSpeed.length; i++) {
            const radius = rtCar.tyreRadius[i];
            const angularSpeed = rtCar.wheelAngularSpeed[i];

            const kph = this.angularSpeedToKph(radius, angularSpeed);
            const synchronization = this.synchronizationRate(
                kph,
                rtCar.speedKph,
            );

            wheels[i] = {
                radius,
                angularSpeed,
                kph,
                synchronization,
            };
        }

        return wheels;
    }

    private angularSpeedToKph(radius: number, angularSpeed: number): number {
        return radius * angularSpeed * 3.6;
    }

    private synchronizationRate(
        wheelSpeedKph: number,
        speedKph: number,
    ): number {
        return speedKph > 0.1
            ? ((Math.abs(wheelSpeedKph) - speedKph) / speedKph) * 100
            : 0;
    }
}
