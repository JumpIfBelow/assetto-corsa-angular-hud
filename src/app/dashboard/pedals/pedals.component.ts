import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-pedals',
    templateUrl: './pedals.component.html',
    styleUrls: ['./pedals.component.scss'],
})
export class PedalsComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public rtCar!: RtCar;

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => (this.rtCar = rtCar));
    }
}
