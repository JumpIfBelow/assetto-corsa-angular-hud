import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-electronics',
    templateUrl: './electronics.component.html',
    styleUrls: ['./electronics.component.scss'],
})
export class ElectronicsComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public rtCar!: RtCar;

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => (this.rtCar = rtCar));
    }
}
