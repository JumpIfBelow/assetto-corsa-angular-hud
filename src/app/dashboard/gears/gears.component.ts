import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-gears',
    templateUrl: './gears.component.html',
    styleUrls: ['./gears.component.scss'],
})
export class GearsComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;

    public gearName: string = 'N';

    constructor() {}

    ngOnInit() {
        this.rtCar$.subscribe(
            (rtCar) => (this.gearName = this.gearToGearName(rtCar.gear)),
        );
    }

    private gearToGearName(gear: number): string {
        if (gear === 0) {
            return 'R';
        }

        if (gear === 1) {
            return 'N';
        }

        return (gear - 1).toString();
    }
}
