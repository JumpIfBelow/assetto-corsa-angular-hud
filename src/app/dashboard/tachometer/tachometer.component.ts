import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';
import { GaugeType } from '../../hud/gauge/gauge-type';

@Component({
    selector: 'app-tachometer',
    templateUrl: './tachometer.component.html',
    styleUrls: ['./tachometer.component.scss'],
})
export class TachometerComponent implements OnInit {
    @Input({ required: true }) rtCar$!: Observable<RtCar>;
    @Input({ required: true }) resetStatistics$!: Observable<void>;

    public rtCar!: RtCar;
    public maxEngineRPM: number;

    public readonly GaugeType = GaugeType;

    constructor() {
        this.maxEngineRPM = 0;
    }

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => {
            this.rtCar = rtCar;
            if (rtCar && rtCar.engineRPM > this.maxEngineRPM) {
                this.maxEngineRPM = rtCar.engineRPM;
            }
        });

        this.resetStatistics$.subscribe(() => (this.maxEngineRPM = 0));
    }
}
