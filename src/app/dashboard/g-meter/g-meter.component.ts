import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RtCar } from '../../model/rt-car';

@Component({
    selector: 'app-g-meter',
    templateUrl: './g-meter.component.html',
    styleUrls: ['./g-meter.component.scss'],
})
export class GMeterComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;
    @Input({ required: true }) public resetStatistics$!: Observable<void>;

    public readonly Math = Math;

    public maxXG: number = 0;
    public maxYG: number = 0;
    public maxZG: number = 0;
    public totalG: number = 0;
    public maxTotalG: number = 0;

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => {
            const absXG = Math.abs(rtCar.verticalG);
            const absYG = Math.abs(rtCar.horizontalG);
            const absZG = Math.abs(rtCar.frontalG);

            this.totalG = absXG + absYG + absZG;

            if (absXG > this.maxXG) {
                this.maxXG = absXG;
            }

            if (absYG > this.maxYG) {
                this.maxYG = absYG;
            }

            if (absZG > this.maxZG) {
                this.maxZG = absZG;
            }

            if (this.totalG > this.maxTotalG) {
                this.maxTotalG = this.totalG;
            }
        });

        this.resetStatistics$.subscribe(() => {
            this.maxXG = 0;
            this.maxYG = 0;
            this.maxZG = 0;
            this.maxTotalG = 0;
        });
    }
}
