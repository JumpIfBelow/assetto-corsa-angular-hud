import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { RtCar } from '../../model/rt-car';
import { LapTime } from '../../model/lap-time';

@Component({
    selector: 'app-timetable',
    templateUrl: './timetable.component.html',
    styleUrls: ['./timetable.component.scss'],
})
export class TimetableComponent implements OnInit {
    @Input({ required: true }) public rtCar$!: Observable<RtCar>;
    @Input({ required: true }) public resetStatistics$!: Observable<void>;

    @ViewChild(MatTable, { static: true }) public matTable!: MatTable<any>;
    @ViewChild(MatPaginator, { static: true })
    public matPaginator!: MatPaginator;

    public lastLapCount!: number;
    public bestLap?: LapTime;
    public lastLap?: LapTime;
    public lapTimes!: LapTime[];
    public sortedLapTimes!: LapTime[];

    public displayedColumns: string[];

    private sort?: Sort;

    constructor() {
        this.initTimetable();

        this.displayedColumns = [
            'index',
            'lap',
            'time',
            'dPrevious',
            'dBest',
            'dLast',
        ];
    }

    ngOnInit() {
        this.rtCar$.subscribe((rtCar) => {
            if (this.lastLapCount !== rtCar.lapCount) {
                this.update(rtCar);
            }
        });

        this.resetStatistics$.subscribe(() => this.initTimetable());
    }

    public updateSort(sort: Sort): void {
        this.sort = sort;
        this.sortLapTimes();
    }

    private update(rtCar: RtCar): void {
        if (rtCar.lastLap === 0) {
            return;
        }

        const currentLap = new LapTime(
            this.lapTimes.length,
            rtCar.lapCount,
            rtCar.lastLap,
        );

        currentLap.previousLap = this.lapTimes[this.lapTimes.length - 1];

        this.lastLapCount = rtCar.lapCount;
        this.lastLap = currentLap;

        this.lapTimes.push(currentLap);

        if (!this.bestLap || this.bestLap.duration !== rtCar.bestLap) {
            const bestIndex = this.lapTimes
                .map((lapTime) => lapTime.duration)
                .indexOf(rtCar.bestLap);
            this.bestLap =
                bestIndex !== -1
                    ? this.lapTimes[bestIndex]
                    : new LapTime(-1, -1, rtCar.bestLap);
        }

        for (const lapTime of this.lapTimes) {
            lapTime.bestLap = this.bestLap;
            lapTime.lastLap = this.lastLap;
        }

        this.sortLapTimes();
    }

    private initTimetable(): void {
        this.lastLapCount = 0;
        this.lapTimes = [];
        this.sortedLapTimes = [];
    }

    private sortLapTimes(): void {
        if (!this.sort || !this.sort.active || !this.sort.direction) {
            this.sortedLapTimes = this.lapTimes;
            return;
        }

        this.sortedLapTimes = this.lapTimes.slice().sort((a, b) => {
            const order = this.sort?.direction === 'asc' ? 1 : -1;
            switch (this.sort?.active) {
                case 'index':
                    return order * this.twc(a.index, b.index);
                case 'lap':
                    return order * this.twc(a.lap, b.lap);
                case 'time':
                    return order * this.twc(a.duration, b.duration);
                case 'dPrevious':
                    return (
                        order *
                        this.twc(a.getDeltaPrevious(), b.getDeltaPrevious())
                    );
                case 'dBest':
                    return order * this.twc(a.getDeltaBest(), b.getDeltaBest());
                case 'dLast':
                    return order * this.twc(a.getDeltaLast(), b.getDeltaLast());
                default:
                    return 0;
            }
        });

        this.matTable.renderRows();
    }

    private twc(a: any, b: any): -1 | 0 | 1 {
        if (a === undefined && b !== undefined) {
            return -1;
        }

        if (a !== undefined && b === undefined) {
            return 1;
        }

        if (a < b) {
            return -1;
        }

        if (a > b) {
            return 1;
        }

        return 0;
    }
}
